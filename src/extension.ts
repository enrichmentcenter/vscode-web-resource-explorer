import * as vscode from 'vscode';
import * as path from 'path';
import * as lineNumber from 'line-number'; 
import {parsePlugins, extractPluginKey, findClosestPomFile, resourceTypes} from './utils';

class ResourceProvider implements vscode.TreeDataProvider<WebResourceItem> {
	webResources?: any[];
	resourceTypes: string[];

	private _onDidChangeTreeData: vscode.EventEmitter<WebResourceItem | undefined> = new vscode.EventEmitter<WebResourceItem | undefined>();
	readonly onDidChangeTreeData: vscode.Event<WebResourceItem | undefined> = this._onDidChangeTreeData.event;

	constructor(resourceTypes: string | string[]) {
		this.resourceTypes = Array.isArray(resourceTypes) ? resourceTypes : [resourceTypes];

		this.loadData(this.resourceTypes);
	}

	loadData(resourceTypes: string[]) {
		parsePlugins(resourceTypes).then((webResources: any) => {
			this.webResources = webResources;

			this._onDidChangeTreeData.fire();
		});
	}

	getTreeItem(element: WebResourceItem): vscode.TreeItem {
		return element;
	}

	getChildren(element: WebResourceItem): Thenable<WebResourceItem[]> {
		if (!this.webResources) {
			vscode.window.showInformationMessage('No web-resource to explore.');
			return Promise.resolve([]);
		}

		// Web-resource resources tree
		if (element) {
			const {label} = element;
			const webResource = this.webResources.find(webResource => webResource._attr.key === label);

			const resourcesDeclarations = Array.isArray(webResource.resource) ? webResource.resource : [webResource.resource];

			const resources = resourcesDeclarations.map((resource: any) => {
				return new WebResourceResource(
					resource._attr.location,
					vscode.TreeItemCollapsibleState.None,
					path.join(webResource.pluginFolder, resource._attr.location)
				);
			});

			return Promise.resolve(resources);
		}

		// Web-resources tree
		const webResourceItems = this.webResources.map(webResource => {
			const webResourceOptions = {
				path: webResource.pluginFolder
			};
	
			const hasResources = Boolean(webResource.resource);

			return new WebResource(
				webResource._attr.key,
				hasResources ? vscode.TreeItemCollapsibleState.Collapsed : vscode.TreeItemCollapsibleState.None,
				webResourceOptions
			);
		});

		return Promise.resolve(webResourceItems);
	}
}

type WebResourceItem = WebResource | WebResourceResource;

class WebResource extends vscode.TreeItem {
	path?: string;

	constructor(
		public readonly label: string,
		public readonly collapsibleState: vscode.TreeItemCollapsibleState,
		options: {
			path: string;
		}
	) {
		super(label, collapsibleState);

		if (options) {
			this.path = options.path;
		}

		// add command if resource isn't openable
		if (this.collapsibleState === vscode.TreeItemCollapsibleState.None) {
			this.command = {
				command: 'webResourceExplorer.openFile',
				title: '',
				arguments: [this]
			};
		}
	}

	contextValue = 'webResource';
}

class WebResourceResource extends vscode.TreeItem {
	command?: vscode.Command;
	resourceUri: vscode.Uri;

	constructor(
		public readonly label: string,
		public readonly collapsibleState: vscode.TreeItemCollapsibleState,
		resourcePath: string
	) {
		super(label, collapsibleState);

		this.command = {
			command: 'extension.openWebResourceResource',
			title: '',
			arguments: [resourcePath]
		};

		this.resourceUri = vscode.Uri.file(resourcePath);
	}
}

class ContextProvider implements vscode.TreeDataProvider<ContextItem> {
	webResources: any[] = [];
	contexts: {
		[label: string]: string[];
	} = {};

	private _onDidChangeTreeData: vscode.EventEmitter<ContextItem | undefined> = new vscode.EventEmitter<ContextItem | undefined>();
	readonly onDidChangeTreeData: vscode.Event<ContextItem | undefined> = this._onDidChangeTreeData.event;
	
	constructor(resourceTypes: string[] | string) {
		resourceTypes = Array.isArray(resourceTypes) ? resourceTypes : [resourceTypes];

		this.loadData(resourceTypes);
	}

	loadData(resourcesType: string[]) {
		parsePlugins(resourcesType).then((webResources: any) => {
			this.webResources = webResources;

			for (let webResource of webResources) {
				let {context} = webResource; 

				if (context) {
					if (!Array.isArray(context)) {
						context = [context];
					}

					for (let contextLabel of context) {
						if (!this.contexts[contextLabel]) {
							this.contexts[contextLabel] = [];
						}

						this.contexts[contextLabel].push(webResource._attr.key);
					}
				}
			}
			
			this._onDidChangeTreeData.fire();
		});
	}

	getTreeItem(element: WebResourceItem): vscode.TreeItem {
		return element;
	}

	getChildren(element: ContextItem): Thenable<ContextItem[]> {
		const contextsKeys = Object.keys(this.contexts);

		if (!contextsKeys) {
			return Promise.resolve([]);
		}

		if (element) {
			const {label: context} = element;
			const webResourceKeys = this.contexts[context];

			const webResourcesItems = webResourceKeys.map(webResourceKey => {
				const webResource = this.webResources.find(webResource => webResource._attr.key === webResourceKey);

				return new WebResource(webResourceKey, vscode.TreeItemCollapsibleState.None, {path: webResource.pluginFolder});
			});

			return Promise.resolve(webResourcesItems);
		}

		const contextItems = contextsKeys.map(context => new Context(context, vscode.TreeItemCollapsibleState.Collapsed));

		return Promise.resolve(contextItems);
	}
}

type ContextItem = Context | WebResource;

class Context extends vscode.TreeItem {
	constructor(
		public readonly label: string,
		public readonly collapsibleState: vscode.TreeItemCollapsibleState
	) {
		super(label, collapsibleState);
	}
}

export async function activate(context: vscode.ExtensionContext) {
	const webResourceProvider = new ResourceProvider(resourceTypes.webResource);
	const clientResourceProvider = new ResourceProvider(resourceTypes.clientResource);
	const contextProvider = new ContextProvider([resourceTypes.webResource, resourceTypes.clientResource]);

	vscode.window.registerTreeDataProvider('webResourceExplorer', webResourceProvider);
	vscode.window.registerTreeDataProvider('clientResourceExplorer', clientResourceProvider);
	vscode.window.registerTreeDataProvider('contextExplorer', contextProvider);

	registerCommands();
}

export function deactivate() {}

function registerCommands() {
	vscode.commands.registerCommand('extension.openWebResourceResource',
		resourceLocation => {
			vscode.workspace.openTextDocument(resourceLocation)
				.then(doc => vscode.window.showTextDocument(doc));
		}
	);

	vscode.commands.registerCommand('webResourceExplorer.openFile',
		webResource => {
			const atlassianPluginPath = path.join(webResource.path, 'atlassian-plugin.xml');
			const {label: webResourceKey} = webResource;
			const re = new RegExp(` key="${webResourceKey}"`);

			vscode.workspace.openTextDocument(atlassianPluginPath)
				.then(doc => {
					const {number: line} = lineNumber(doc.getText(), re)[0] || {number: 0};
					const startPosition = new vscode.Position(line - 1, 0);
					const endPosition = new vscode.Position(line, 0);
					const range = new vscode.Range(startPosition, endPosition);

					vscode.window.showTextDocument(doc, {
						selection: range
					});
				});
		}
	);

	vscode.commands.registerCommand('webResourceExplorer.copyKey',
		webResource => {
			const {path} = webResource;
			const resourceDeclarationFolder = path.replace(vscode.workspace.rootPath, '').split('/').filter(Boolean);

			findClosestPomFile(resourceDeclarationFolder)
				.then((pomFilePath: any) => {
					if (!pomFilePath) {
						throw new Error('Can not find pom.xml file');
					}

					return vscode.workspace.openTextDocument(pomFilePath);
				})
				.then(doc => {
					const content = doc.getText();
					const pluginKey = extractPluginKey(content);
					const webResourceKey = `${pluginKey}:${webResource.label}`;

					return vscode.env.clipboard.writeText(webResourceKey);
				})
				.catch(err => {
					vscode.window.showInformationMessage(err.message);
				});
		}
	);
}