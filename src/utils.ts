import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import {parse as xmlParse} from 'fast-xml-parser';

const ATL_PLUGIN_ROOT = 'atlassian-plugin';

export enum resourceTypes {
    webResource = 'web-resource',
    clientResource = 'client-resource',
}

interface XMLDescriptor {
	[key: string]: any;
}

const XML_PARSER_OPTIONS = {
	attributeNamePrefix: '',
	attrNodeName: '_attr',
	textNodeName: '–text',
	ignoreAttributes: false,
	allowBooleanAttributes: true,
};

function getResourceDeclarations(content: XMLDescriptor, resourceTypes: string[]) {
    const rootDeclaration = content[ATL_PLUGIN_ROOT] || {};
    let resourcesDeclarations: any[] = [];

    for (let resourceType of resourceTypes) {
        let resources = rootDeclaration[resourceType] || [];

        // if parser found only one declaration it'll return an object instead array
        if (!Array.isArray(resources)) {
            resources = [resources];
        }

        resourcesDeclarations = resourcesDeclarations.concat(resources);
    }

    return resourcesDeclarations;
}

export const parsePlugins = (resourceTypes: string[]) => {
    let resources: any[] = [];

    return vscode.workspace.findFiles('**/atlassian-plugin.xml')
        .then((files: any[]) => {
            for (let file of files) {
                if (/\/target\//.test(file.path)) {
                    continue;
                }

                const pluginFolder = file.path.replace('atlassian-plugin.xml', '');
                const fileContent = fs.readFileSync(file.path, 'utf-8');
                const parsedContent = xmlParse(fileContent, XML_PARSER_OPTIONS);

                const resourceDeclarations = getResourceDeclarations(parsedContent, resourceTypes).map((resourceDeclarations: any) => ({
                    ...resourceDeclarations,
                    pluginFolder
                }));

                resources = resources.concat(resourceDeclarations);
            }

            return resources;
        });
};

export const extractPluginKey = (pomContent: string = '') => {
    const parsedContent = xmlParse(pomContent, XML_PARSER_OPTIONS);

    return `${parsedContent.project.groupId}.${parsedContent.project.artifactId}`;
};


export const findClosestPomFile = (folder: string[]) => {
    return new Promise((resolve) => {
        if (typeof folder === 'undefined') {
            return resolve(null);
        }

        folder = folder.slice();

        const rootPath = vscode.workspace.rootPath || '';
        const pathToCheck = path.join(rootPath, ...folder, 'pom.xml');

        fs.access(pathToCheck, fs.constants.F_OK, err => {
            if (err) {
                if (folder.length === 0) {
                    return resolve(null);
                }

                folder.pop();
                return resolve(findClosestPomFile(folder));
            }

            return resolve(pathToCheck);
        });
    });
};
