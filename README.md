# Atlassian Web-Resource Explorer
Web-resource explorer is a vscode extension that makes life easier.

## Feature
* web-resource explorer
* client-resource explorer
* context explorer
* Go to web-resource/client-resource definition
* Copy web-resource

## Bug reports
Report problems with extension [here](https://bitbucket.org/enrichmentcenter/vscode-web-resource-explorer/issues?status=new&status=open).